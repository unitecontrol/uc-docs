# UC MO-ET-SL {.title}

[ RS485 IO Slim Module MO-ET-SL  ](http://pl.unitecontrol.com/MO-ET-SL.html)Moduł rozszerzający – brama Modbus TCP  

## Zasady bezpieczeństwa

* Przed pierwszym uruchomieniem urządzenia należy zapoznać się z niniejszą instrukcją obsługi;
* Przed pierwszym uruchomieniem urządzenia należy upewnić się, że wszystkie przewody zostały podłączone prawidłowo;
* Należy zapewnić właściwe warunki pracy, zgodne ze specyfikacją urządzenia (np.: napięcie zasilania, temperatura, maksymalny pobór prądu);
* Przed dokonaniem jakichkolwiek modyfikacji przyłączeń przewodów, należy wyłączyć napięcie zasilania.

## Charakterystyka modułu

### Przeznaczenie i opis modułu

* Moduł ETH jest innowacyjnym urządzeniem konwertującym Modbus TCPdo Modbus RTU/ASCII.
* Urządzenie posiada interfejs Ethernet i RS485, 4 wejścia cyfrowe z licznikami oraz3 wyjścia przekaźnikowe. Wszystkie wejścia są izolowane od logiki za pomocą transoptorów.
* Komunikacja odbywa się z wykorzystaniem protokołu Modbus TCP. Każde odebrane zapytanie od klienta Modbus TCP jest sprawdzane pod kątem adresu. Jeśli adres jest różny od adresu urządzenia MO-ET-SL, to następuje automatyczna konwersja ramki zapytania do protokołu Modbus RTU/ASCII i czekanie na odpowiedź, która po prawidłowym odebraniu jest wysyłana do klienta Modbus TCP. Zastosowanie32-bitowego procesora z rdzeniem ARM zapewnia szybkie przetwarzanie danychi szybką komunikację.
* Nowością jest funkcja Modbus Device Table, która pozwala użytkownikowi na zdefiniowanie własnych zapytań po Modbus RTU/ASCII z dostępnych rejestrów wewnętrznych urządzenia. Funkcjonalność taka pozwala np. na automatyczne odczytywanie stanów wejść modułów na RS485 i wpisanie tego stanu do rejestrów wewnętrznych MO-ET-SL. Rejestry wewnętrzne są dostępne dla klientów Modbus TCP bez dodatkowych opóźnień wynikających z magistrali RS485. Rozwiązanie takie stanowczo przyspiesza komunikację. Dostępne są wszystkie rozkazy bitowe i rejestrowe protokołu MODBUS.
* Moduł przeznaczony jest do montażu na szynie DIN zgodnie z normą DIN EN 5002.
* Moduł został wyposażony w zestaw diod LED (kontrolek), używanych do wskazywania stanu wyjść przydatnych w celach diagnostycznych i pomagających w znalezieniu błędów.
* Konfiguracja modułu odbywa się przez wbudowaną stronę www lub przez USB za pomocą dedykowanego programu komputerowego. Możliwa jest również zmiana parametrów za pomocą protokołu Modbus.

### Specyfikacja techniczna

<table>
  <tr>
    <th rowspan="2" bgcolor="#e6e6ff"><b>Zasilanie</b>
    </th>
    <th>Napięcie</th>
    <th>12-24 V AC/DC ± 20%</th>
  </tr>
  <tr>
    <th>Prąd maksymalny</th>
    <th>360 mA @ 12V / 300 mA @ 24V</th>
  </tr>
  <tr>
    <td rowspan="7" bgcolor="#e6e6ff"><b>Wejścia cyfrowe</b>
    </td>
    <td>Liczba wejść</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Zakres napięć</td>
    <td>0 - 36V</td>
  </tr>
  <tr>
    <td>Stan niski „0”</td>
    <td>0 - 3V</td>
  </tr>
  <tr>
    <td>Stan wysoki „1”</td>
    <td>4 - 36V</td>
  </tr>
  <tr>
    <td>Impedancja wejściowa </td>
    <td>4kΩ</td>
  </tr>
  <tr>
    <td>Izolacja</td>
    <td>3750 Vrms</td>
  </tr>
  <tr>
    <td>Typ wejść</td>
    <td>PNP lub NPN</td>
  </tr>
  <tr>
    <td rowspan="4" bgcolor="#e6e6ff"><b>Liczniki</b>
    </td>
    <td>Ilość</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Rozdzielczość</td>
    <td>32 bity</td>
  </tr>
  <tr>
    <td>Częstotliwość</td>
    <td>1kHz (max)</td>
  </tr>
  <tr>
    <td>Szerokość impulsu</td>
    <td>500 µs (min)</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wyjścia przekaźnikowe</b>
    </td>
    <td>Liczba wyjść</td>
    <td>3</td>
  </tr>
  <tr>
    <td rowspan="2">Maksymalny prąd i napięcie
      <br>(obciążenie rezystancyjne)</td>
    <td>3A 230VAC</td>
  </tr>
  <tr>
    <td>3A 30VDC</td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Temperatura</b>
    </td>
    <td>Pracy</td>
    <td>-20 °C - +65°C</td>
  </tr>
  <tr>
    <td>Przechowywania</td>
    <td>-40 °C - +85°C</td>
  </tr>
  <tr>
    <td rowspan="6" bgcolor="#e6e6ff"><b>Złącza</b>
    </td>
    <td>Zasilające</td>
    <td>2 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne RS485</td>
    <td>3 pinowe</td>
  </tr>
  <tr>
    <td>Komunikacyjne Ethernet</td>
    <td>RJ45</td>
  </tr>
  <tr>
    <td>Wejścia i wyjścia</td>
    <td>2 x 5 pinowe</td>
  </tr>
  <tr>
    <td>Szybkozłączka</td>
    <td>IDC10 </td>
  </tr>
  <tr>
    <td>Konfiguracyjne</td>
    <td>Mini USB</td>
  </tr>
  <tr>
    <td rowspan="3" bgcolor="#e6e6ff"><b>Wymiary</b>
    </td>
    <td>Wysokość</td>
    <td>120 mm</td>
  </tr>
  <tr>
    <td>Głębokość</td>
    <td>110 mm</td>
  </tr>
  <tr>
    <td>Szerokość</td>
    <td>22,5 mm</td>
  </tr>
  <tr>
    <td rowspan="2" bgcolor="#e6e6ff"><b>Interfejs</b>
    </td>
    <td>Ethernet</td>
    <td>10/100 Mbps</td>
  </tr>
  <tr>
    <td>RS485</td>
    <td>Do 128 urządzeń</td>
  </tr>
</table>

### Wymiary modułu

![](images/MO-ET-SL/overview.png) {.img-responsive}

Wygląd i wymiary modułu znajdują się na rysunku poniżej. Moduł mocowany jest bezpośrednio do szyny w przemysłowym standardzie DIN. Złącza zasilające, komunikacyjne oraz wejść znajdują się od dołu i góry modułu. Złącze konfiguracyjne USB oraz wskaźniki znajdują się z przodu modułu.

 

## Konfiguracja komunikacji

### Uziemienie i ekranowanie

W większości przypadków, moduł będzie zainstalowany w obudowie wraz z innymi urządzeniami, które generują promieniowanie elektromagnetyczne. Przykładami takich urządzeń są przekaźniki i styczniki, transformatory, sterowniki silników itp. To promieniowanie elektromagnetyczne może powodować zakłócenia elektryczne zasilania i przewodów sygnałowych, a także promieniując bezpośrednio do modułu, powodując negatywne skutki dla systemu. Odpowiednie uziemienie, osłony oraz inne działania ochronne należy podjąć na etapie instalacji, aby zapobiec tym efektom. Te działania ochronne obejmują m.in. uziemienie szafy sterowniczej, uziemienie modułu, uziemienie ekranowania przewodów, zabezpieczenie urządzeń przełączających, prawidłowego okablowania, jak również uwzględnienie typów kabli i ich przekrojów. 

### Terminator

Efekty linii transmisyjnej często powodują problemy w sieciach teleinformatycznych. Problemy te dotyczą najczęściej tłumienia sygnału i odbić w sieci.

Aby wyeliminować obecność odbić od końców kabla, należy na obu jego końcach zastosować rezystor o impedancji równej impedancji charakterystycznej linii. W przypadku skrętki RS485 typową wartością jest 120 Ω.

### Ustalanie adresu modułu w sieci

Zmiana adresu modułu MO-ET-SL odbywa się poprzez wbudowaną stronę www.Po uprzednim zalogowaniu na stronę należy wybrać zakładkę Network i w polu Device Address wpisać adres modułu i kliknąć Save. Urządzenie zapisze wówczas podany adres i będzie go pamiętać nawet po odłączeniu zasilania (szczegóły w - Modbus Config).

**Uwaga!**Adres jest resetowany podczas przywracania konfiguracji domyślnej (szczegóły w – Domyślne parametry).

### Typy rejestrów Modbus

Są 4 typy zmiennych dostępnych w module. 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Typ</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres początkowy</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Zmienna</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Dostęp</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Rozkaz Modbus</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>00001</td>
    <td>Wyjścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt i zapis</td>
    <td>1, 5, 15</td>
  </tr>
  <tr>
    <td>2</td>
    <td>10001</td>
    <td>Wejścia cyfrowe</td>
    <td>Bitowy
      <br>Odczyt</td>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>30001</td>
    <td>Rejestry wejściowe</td>
    <td>Rejestrowy
      <br>Odczyt</td>
    <td>3</td>
  </tr>
  <tr>
    <td>4</td>
    <td>40001</td>
    <td>Rejestry wyjściowe</td>
    <td>Rejestrowy
      <br>Odczyt i zapis</td>
    <td>4, 6, 16</td>
  </tr>
</table>

### Ustawienia komunikacji

Ustawienia komunikacji TCP są przechowywane w nieulotnej pamięci urządzenia. Konfiguracja sieci Modbus TCP dostępna jest wyłącznie poprzez stronę www. (szczegóły w - Network) Dane komunikacji modułu w sieci RS485 przechowywane są w 16 bitowych rejestrach. Dostęp do rejestrów odbywa się za pomocą protokołu Modbus TCP lub poprzez stronę www (szczegóły w - Modbus Config).

#### Domyślne parametry

Domyślną konfigurację można przywrócić za pomocą przełącznika SW6 (szczegóły w - Przywracanie konfiguracji domyślnej).

<table>
  <tr>
    <td colspan="2" bgcolor="#e6e6ff"><b>Modbus TCP</b>
    </td>
    <td colspan="2" bgcolor="#e6e6ff"><b>Modbus RTU/ASCII</b>
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres IP</b>
    </td>
    <td bgcolor="#ffffff">192.168.1.135</td>
    <td bgcolor="#e6e6ff"><b>Prędkość transmisji</b>
    </td>
    <td>19200</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Maska</b>
    </td>
    <td bgcolor="#ffffff">255.255.255.0</td>
    <td bgcolor="#e6e6ff"><b>Parzystość</b>
    </td>
    <td>Nie</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Brama</b>
    </td>
    <td bgcolor="#ffffff">192.168.1.1</td>
    <td bgcolor="#e6e6ff"><b>Ilość bitów danych</b>
    </td>
    <td>8</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Port Modbus</b>
    </td>
    <td bgcolor="#ffffff">502</td>
    <td bgcolor="#e6e6ff"><b>Ilość bitów stopu</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Port HTTP</b>
    </td>
    <td bgcolor="#ffffff">80</td>
    <td bgcolor="#e6e6ff"><b>Tryb Modbus</b>
    </td>
    <td>RTU</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Timeout połączenia</b>
    </td>
    <td bgcolor="#ffffff">60 s</td>
    <td bgcolor="#e6e6ff"><b>Adres urządzenia</b>
    </td>
    <td>1</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Login</b>
    </td>
    <td bgcolor="#ffffff">admin</td>
    <td bgcolor="#e6e6ff"><b>Timeout na RS485</b>
    </td>
    <td>500 ms</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Hasło</b>
    </td>
    <td bgcolor="#ffffff">0000</td>
    <td bgcolor="#e6e6ff"><b>Tryb</b>
    </td>
    <td>Brama</td>
  </tr>
  <tr>
    <td rowspan="3" colspan="2" bgcolor="#e6e6ff">
      <br>
    </td>
    <td bgcolor="#e6e6ff"><b>Device Table Refresh Slow</b>
    </td>
    <td>10000 ms</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Device Table Refresh Normal</b>
    </td>
    <td>2000 ms</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff"><b>Device Table Refresh  Fast</b>
    </td>
    <td>500 ms</td>
  </tr>
</table>

#### Przywracanie konfiguracji domyślnej

W celu przywrócenia konfiguracji domyślnej należy przy wyłączonym zasilaniu modułu załączyć przełącznik SW6, a następnie włączyć zasilanie. Moduł zacznie migać na zmianę diodami wskazującymi zasilanie i komunikację. Jeżeli w tym stanie zostanie wyłączony przełącznik SW6 ustawienia zostaną nadpisane. 

**Uwaga!**Podczas przywracania konfiguracji domyślnej wykasowane zostaną również wszystkie inne wartości zapisane w rejestrach modułu!

#### Rejestry konfiguracyjne

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Adres Modbus</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres Dec</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Adres Hex</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Nazwa</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Wartości</b>
    </td>
  </tr>
  <tr>
    <td>40003</td>
    <td>2</td>
    <td>0x02</td>
    <td>Prędkość transmisji</td>
    <td>0 – 2400
      <br>1 – 4800
      <br>2 – 9600
      <br>3 – 19200
      <br>4 – 38400
      <br>5 – 57600
      <br>6 – 115200
      <br>inna wartość – wartość * 10</td>
  </tr>
  <tr>
    <td>40005</td>
    <td>4</td>
    <td>0x04</td>
    <td>Parzystość</td>
    <td>0 – brak
      <br>1 – nieparzystość
      <br>2 – parzystość
      <br>3 – zawsze 1
      <br>4 – zawsze 0</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu LSB</td>
    <td>1 – jeden bit stopu
      <br>2 – dwa bity stopu</td>
  </tr>
  <tr>
    <td>40004</td>
    <td>3</td>
    <td>0x03</td>
    <td>Bity Stopu MSB</td>
    <td>7 – 7 bitów danych
      <br>8 – 8 bitów danych</td>
  </tr>
  <tr>
    <td>40007</td>
    <td>6</td>
    <td>0x06</td>
    <td>Tryb Modbus</td>
    <td>0 – RTU
      <br>1 – ASCII</td>
  </tr>
</table>

## Wskaźniki diodowe

![](images/MO-ET-SL/leds.png) {.img-responsive}

 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Wskaźnik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Zasilanie</td>
    <td>Zapalona dioda oznacza, że moduł jest poprawnie zasilany.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Komunikacja</td>
    <td>Dioda zapala się, gdy moduł odebrał prawidłowy pakiet Modbus TCP, przekonwertował go na Modbus RTU/ASCII i wysłał go poprzez sieć RS485.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Stany wejść</td>
    <td>Zapalona dioda informuje, że wejście jest podłączone.</td>
  </tr>
  <tr>
    <td bgcolor="#e6e6ff">Stany wyjść</td>
    <td>Zapalona dioda informuje, że wyjście jest załączone.</td>
  </tr>
</table>

## Podłączenie modułu

## Ustawienia przełączników

![](images/MO-ET-SL/switch.png) {.img-responsive}

 

<table>
  <tr>
    <td bgcolor="#e6e6ff"><b>Przełącznik</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Funkcja</b>
    </td>
    <td bgcolor="#e6e6ff"><b>Opis</b>
    </td>
  </tr>
  <tr>
    <td>1</td>
    <td>Brak</td>
    <td>
      <br>
    </td>
  </tr>
  <tr>
    <td>2</td>
    <td>Brak</td>
    <td>
      <br>
    </td>
  </tr>
  <tr>
    <td>3</td>
    <td>Bias Pull Up</td>
    <td>Załączenie rezystora podciągającego</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Bias Pull Down</td>
    <td>Załączenie rezystora podciągającego</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Terminator</td>
    <td>Załączenie rezystora terminującego 120Ω</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Ustawienia domyślne modułu</td>
    <td>Ustawienie domyślnych parametrów transmisji
      <br>(patrz - Domyślne parametry i - Przywracanie konfiguracji domyślnej).</td>
  </tr>
</table>

## Rejestry modułu

### Dostęp rejestrowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>30001</td>
      <td>0</td>
      <td>0x00</td>
      <td>Wersja/Typ</td>
      <td>Odczyt</td>
      <td>Typ i wersja urządzenia</td>
    </tr>
    <tr>
      <td>30002</td>
      <td>1</td>
      <td>0x01</td>
      <td>Adres</td>
      <td>Odczyt</td>
      <td>Adres modułu MO-ET-SL</td>
    </tr>
    <tr>
      <td>40003</td>
      <td>2</td>
      <td>0x02</td>
      <td>Prędkość</td>
      <td>Odczyt i zapis</td>
      <td>Prędkość transmisji</td>
    </tr>
    <tr>
      <td>40004</td>
      <td>3</td>
      <td>0x03</td>
      <td>Bity stopu</td>
      <td>Odczyt i zapis</td>
      <td>Ilość bitów stopu</td>
    </tr>
    <tr>
      <td>40005</td>
      <td>4</td>
      <td>0x04</td>
      <td>Parzystość</td>
      <td>Odczyt i zapis</td>
      <td>Bit parzystości</td>
    </tr>
    <tr>
      <td>40007</td>
      <td>6</td>
      <td>0x06</td>
      <td>Typ Modbus</td>
      <td>Odczyt i zapis</td>
      <td>Typ protokołu Modbus </td>
    </tr>
    <tr>
      <td>40009</td>
      <td>8</td>
      <td>0x08</td>
      <td>Watchdog</td>
      <td>Odczyt i zapis</td>
      <td>Funkcja watchdog dla wyjść [ms]</td>
    </tr>
    <tr>
      <td>40013</td>
      <td>12</td>
      <td>0x0C</td>
      <td>Domyślny stan wyjść</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjść
        <br>zapalony bit → wejście włączone</td>
    </tr>
    <tr>
      <td>40014</td>
      <td>13</td>
      <td>0x0D</td>
      <td>Tryb pracy</td>
      <td>Odczyt i zapis</td>
      <td>Tryb Modbus TCP
        <br>0 – Device Table; 1 – Brama Modbus TCP</td>
    </tr>
    <tr>
      <td>40015</td>
      <td>14</td>
      <td>0x0E</td>
      <td>Odpytywanie Slow</td>
      <td>Odczyt i zapis</td>
      <td>Częstotliwość odpytywania 1 w trybie Device Table [ms]</td>
    </tr>
    <tr>
      <td>40016</td>
      <td>15</td>
      <td>0x0F</td>
      <td>Odpytywanie Normal</td>
      <td>Odczyt i zapis</td>
      <td>Częstotliwość odpytywania 2 w trybie Device Table [ms]</td>
    </tr>
    <tr>
      <td>40017</td>
      <td>16</td>
      <td>0x10</td>
      <td>Odpytywanie Fast</td>
      <td>Odczyt i zapis</td>
      <td>Częstotliwość odpytywania 3 w trybie Device Table [ms]</td>
    </tr>
    <tr>
      <td>40033</td>
      <td>32</td>
      <td>0x20</td>
      <td>Odebrane ramki LSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych ramek</td>
    </tr>
    <tr>
      <td>40034</td>
      <td>33</td>
      <td>0x21</td>
      <td>Odebrane ramki MSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40035</td>
      <td>34</td>
      <td>0x22</td>
      <td>Błędne ramki LSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość odebranych błędnych ramek</td>
    </tr>
    <tr>
      <td>40036</td>
      <td>35</td>
      <td>0x23</td>
      <td>Błędne ramki MSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>40037</td>
      <td>36</td>
      <td>0x24</td>
      <td>Wysłane ramki LSB</td>
      <td>Odczyt i zapis</td>
      <td rowspan="2">Ilość wysłanych ramek</td>
    </tr>
    <tr>
      <td>40038</td>
      <td>37</td>
      <td>0x25</td>
      <td>Wysłane ramki MSB</td>
      <td>Odczyt i zapis</td>
    </tr>
    <tr>
      <td>30051</td>
      <td>50</td>
      <td>0x32</td>
      <td>Wejścia</td>
      <td>Odczyt </td>
      <td>Podłączone wejścia
        <br>zapalony bit → wejście podłączone</td>
    </tr>
    <tr>
      <td>40052</td>
      <td>51</td>
      <td>0x33</td>
      <td>Wyjścia</td>
      <td>Odczyt i zapis</td>
      <td>Wyjścia alarmowe
        <br>bit 8 i 9 wyjścia cyfrowe</td>
    </tr>
    <tr>
      <td>30053</td>
      <td>52</td>
      <td>0x34</td>
      <td>Licznik 0 LSB</td>
      <td>Odczyt</td>
      <td rowspan="2">32 bitowy licznik 0</td>
    </tr>
    <tr>
      <td>30054</td>
      <td>53</td>
      <td>0x35</td>
      <td>Licznik 0 MSB</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30055</td>
      <td>54</td>
      <td>0x36</td>
      <td>Licznik 1 LSB</td>
      <td>Odczyt</td>
      <td rowspan="2">32 bitowy licznik 1</td>
    </tr>
    <tr>
      <td>30056</td>
      <td>55</td>
      <td>0x37</td>
      <td>Licznik 1 MSB</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30057</td>
      <td>56</td>
      <td>0x38</td>
      <td>Licznik 2 LSB</td>
      <td>Odczyt</td>
      <td rowspan="2">32 bitowy licznik 2</td>
    </tr>
    <tr>
      <td>30058</td>
      <td>57</td>
      <td>0x39</td>
      <td>Licznik 2 MSB</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>30059</td>
      <td>58</td>
      <td>0x3A</td>
      <td>Licznik 3 LSB</td>
      <td>Odczyt</td>
      <td rowspan="2">32 bitowy licznik 3</td>
    </tr>
    <tr>
      <td>30060</td>
      <td>59</td>
      <td>0x3B</td>
      <td>Licznik 3 MSB</td>
      <td>Odczyt</td>
    </tr>
    <tr>
      <td>40061</td>
      <td>60</td>
      <td>0x3C</td>
      <td>Reset liczników</td>
      <td>Odczyt i zapis</td>
      <td>Resetowanie liczników
        <br>zapalony bit → reset licznika</td>
    </tr>
  </tbody>
</table>

### Dostęp bitowy

<table>
  <thead>
    <tr>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Modbus</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Dec</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Adres</b>
        <br><b>Hex</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Nazwa rejestru</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Dostęp</b>
      </td>
      <td bgcolor="#e6e6ff"><b>Opis</b>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>193</td>
      <td>192</td>
      <td>0x0C0</td>
      <td>Domyślny stan wyjścia 1</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 1</td>
    </tr>
    <tr>
      <td>194</td>
      <td>193</td>
      <td>0x0C1</td>
      <td>Domyślny stan wyjścia 2</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 2</td>
    </tr>
    <tr>
      <td>195</td>
      <td>194</td>
      <td>0x0C2</td>
      <td>Domyślny stan wyjścia 3</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 3</td>
    </tr>
    <tr>
      <td>196</td>
      <td>195</td>
      <td>0x0C3</td>
      <td>Domyślny stan wyjścia 4</td>
      <td>Odczyt i zapis</td>
      <td>Domyślny stan wyjścia 4</td>
    </tr>
    <tr>
      <td>801</td>
      <td>800</td>
      <td>0x320</td>
      <td>Wejście 1 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>802</td>
      <td>801</td>
      <td>0x321</td>
      <td>Wejście 2</td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>803</td>
      <td>802</td>
      <td>0x322</td>
      <td>Wejście 3 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>804</td>
      <td>803</td>
      <td>0x323</td>
      <td>Wejście 4 </td>
      <td>Odczyt</td>
      <td>Czy podłączone wejście</td>
    </tr>
    <tr>
      <td>817</td>
      <td>818</td>
      <td>0x332</td>
      <td>Wyjście cyfrowe 1 </td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia cyfrowego 1</td>
    </tr>
    <tr>
      <td>818</td>
      <td>819</td>
      <td>0x333</td>
      <td>Wyjście cyfrowe 2</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia cyfrowego 2</td>
    </tr>
    <tr>
      <td>819</td>
      <td>820</td>
      <td>0x334</td>
      <td>Wyjście cyfrowe 3</td>
      <td>Odczyt i zapis</td>
      <td>Stan wyjścia cyfrowego 3</td>
    </tr>
    <tr>
      <td>993</td>
      <td>994</td>
      <td>0x3E0</td>
      <td>Reset Licznika 0</td>
      <td>Odczyt i zapis</td>
      <td>Reset Licznika 0</td>
    </tr>
    <tr>
      <td>994</td>
      <td>995</td>
      <td>0x3E1</td>
      <td>Reset Licznika 1</td>
      <td>Odczyt i zapis</td>
      <td>Reset Licznika 1</td>
    </tr>
    <tr>
      <td>995</td>
      <td>996</td>
      <td>0x3E2</td>
      <td>Reset Licznika 2</td>
      <td>Odczyt i zapis</td>
      <td>Reset Licznika 2</td>
    </tr>
    <tr>
      <td>996</td>
      <td>997</td>
      <td>0x3E3</td>
      <td>Reset Licznika 3</td>
      <td>Odczyt i zapis</td>
      <td>Reset Licznika 3</td>
    </tr>
  </tbody>
</table>

## Program konfiguracyjny

![](images/MO-ET-SL/configurator.png) {.img-responsive}

Konfigurator jest oprogramowaniem służącym do ustawienia rejestrów odpowiedzialnych za komunikację modułu w magistrali Modbus RTU/ASCII jak również do odczytu i zapisu aktualnych wartości pozostałych rejestrów modułu. Dzięki temu programowi można w wygodny sposób przetestować układ jak również w czasie rzeczywistym obserwować zmiany w rejestrach.

Komunikacja z modułem odbywa się poprzez kabel USB. Do współdziałania programu z modułem nie jest wymagana instalacja żadnych sterowników.

Konfigurator jest uniwersalnym programem, za pomocą którego możliwa jest konfiguracja wszystkich dostępnych modułów.

 

## Strona www

Urządzenie MO-ET-SL posiada wbudowaną stronę www, dzięki której użytkownik może kontrolować jego pracę. Na stronie dostępna jest konfiguracja sieci TCP oraz RS485, aktualne stany wejść i wyjść oraz ustawienia trybu Device Table.

### Logowanie

Dostęp do strony odbywa się poprzez przeglądarkę. W polu adresu strony należy wpisać adres IP urządzenia i nacisnąć Enter. Wówczas pojawi się strona logowania, na której należy wpisać login 'admin' oraz zdefiniowane hasło (domyślnie '0000'). Jeśli login oraz hasło są poprawne, po kliknięciu przycisku Login wyświetla się domyślna strona www z otwartą zakładką Info.

Wylogowanie nastąpi, jeśli strona domyślna będzie otwarta przez co najmniej 15 min na jednej z zakładek Info, Network, Modbus Config lub po kliknięciu przycisku Logout.

### Info

Zakładka Info zawiera odnośniki do instrukcji urządzenia oraz informację o aktualnej wersji oprogramowania. Możliwa jest również zmiana hasła dostępu do strony www.

W celu zmiany hasła należy wpisać aktualne hasło w polu Old Password oraz nowe w polach New Password i Confirm Password, następnie kliknąć przycisk Change Password.

**Uwaga!**Po przywróceniu ustawień domyślnych hasło jest resetowane (szczegóły w - Domyślne parametry).

### Network

Zakładka Network służy do konfiguracji sieci Modbus TCP. Znajdują się w niej następujące parametry:

* IP – adres IP modułu,
* Mask – maska sieci,
* Gateway – brama,
* Modbus Port – port do połączenia PC z Modbusem TCP,
* HTTP Port – port do połączenia ze stroną www,
* Connection Timeout – maksymalny czas oczekiwania na zapytania po Modbusie TCP, po którym połączenie na porcie Modbus Port zostanie rozłączone (podanyw sekundach).

Aby zatwierdzić zmiany należy kliknąć przycisk Save. W przypadku zmiany parametrów IP, Mask, Gateway, Modbus Port i/lub HTTP Port należy zresetować moduł, aby zmiany zostały wprowadzone. W tym celu umieszczono przycisk Reset Device, który zdalnie zresetuje urządzenie. Po jego kliknięciu przeglądarka spróbuje nawiązać połączenie z nowym adresem IP i po kilku sekundach zostanie załadowana strona logowania z aktualnym adresem IP.

**Uwaga!**Po przywróceniu ustawień domyślnych wszystkie parametry są resetowane (szczegóły w - Domyślne parametry).

### Modbus Config

W zakładce Modbus Config możliwa jest konfiguracja podstawowych parametrów sieci Modbus dla obu trybów pracy. Znalazły się tu parametry sieci RS485 do komunikacji z zewnętrznymi modułami (szczegóły w - Rejestry konfiguracyjne), a także:

* Device Address – adres modułu w sieci Modbus TCP,
* RS485 Timeout – maksymalny czas oczekiwania na odpowiedź w sieci Modbus RTU/ASCII (podany w milisekundach),
* Device Table Refresh Slow, Normal, Fast – częstotliwość odświeżania zapytania w trybie Device Table (podany w milisekundach),
* Mode – tryb pracy modułu (szczegóły w - Tryby pracy Modbus TCP).

### Local I/O

Zakładka Local I/O umożliwia podgląd oraz sterowanie wejściami oraz wyjściami cyfrowymi urządzenia. 

Cztery ikony oznaczone odpowiednio Digital input 1, 2, 3, 4 pokazują aktualny stan wejść cyfrowych. Kolor szary oznacza, że wejście jest nieaktywne, zaś kolor zielony oznacza, że jest ono aktywne. 

Ikony oznaczone Digital output 1, 2, 3 pozwalają na sterowanie wyjściami. Kolor szary oznacza, że dane wyjście jest wyłączone, a kolor pomarańczowy, że jest ono załączone.Po kliknięciu na dany przycisk wysyła ono informację do modułu o stanie wyjść.

Stan wejść i wyjść jest cyklicznie odświeżany, więc powyżej opisane ikony odzwierciedlają aktualny stan wejść i wyjść urządzenia.

W zakładce znajdują się również pola ze stanami 4 liczników, które zliczają impulsy na wejściach 1, 2, 3 oraz 4. Pola są wyłącznie do odczytu, liczniki można jedynie resetować odpowiednimi przyciskami Reset.

Moduł umożliwia również zdefiniowanie domyślnych stanów wyjść. Na stronie www możliwe jest ich ustawienie w sposób analogiczny do wyjść cyfrowych – kolor szary oznacza, że domyślnie wyjście jest wyłączone, a pomarańczowy, że zostanie załączone. Stan domyślny jest przypisywany po włączeniu zasilania oraz po upłynięciu czasu Watchdoga, który resetowany jest po każdym poprawnym pakiecie Modbus TCP adresowanym do modułu MO-ET-SL. Jeśli wartość Watchdoga jest równa zeru, stany domyślne przypisywane są tylko po włączeniu zasilania.

### Device Table

Kolejna zakładka zawiera konfiguracje dla trybu Device Table, która pozwala na zdefiniowanie własnych zapytań po Modbusie RTU/ASCII z dostępnych rejestrów wewnętrznych urządzenia.

Pierwsza zakładka Internal Registers zawiera tabelę rejestrów wewnętrznych modułu MO-ET-SL, które są cyklicznie odświeżane przez stronę www. Rejestry te są wykorzystywane poprzez dodanie zdalnych zapytań w zakładce Devices. Kliknięcie przycisku Add Device powoduje pojawienie się wiersza do zdefiniowania zapytania. Każdy wiersz zawiera następujące informacje:

* Device Address – adres urządzenia w sieci RS485, do którego moduł MO-ET-SL będzie wysyłać zapytanie,
* Function – funkcja Modbus zapytania,
* Size – ilość bitów/rejestrów do odpytania/zapisania,
* Register Address – adres początkowego rejestru,
* Internal Address – początkowy adres rejestru wewnętrznego, gdzie będą przechowywane dane do zapisu/odczytu,
* Speed – wybór jednej z trzech częstotliwości odpytywania (wartości są konfiguralne w rejestrach modułu),
* ON/OFF – zapytanie aktywne lub nieaktywne,
* Status – wyświetla status zapytania.

Konfiguracja zapytań może zostać zapisana przez użytkownika w zewnętrznym pliku oraz później odczytana i automatycznie przywrócona do urządzenia. Konfiguracja zapisywana jest w pamięci nieulotnej urządzenia i zostaje zresetowana tylko przy przywróceniu konfiguracji domyślnej (szczegóły w - Domyślne parametry).

## Tryby pracy Modbus TCP

Moduł MO-ET-SL posiada dwa odmienne tryby pracy. Pierwszy z nich to brama Modbus TCP, w której urządzenie konwertuje ramki Modbus TCP na Modbus RTU/ASCII i przesyła je do urządzeń w sieci RS485.

Drugi tryb to funkcja Device Table, w którym moduł odpytuje urządzenia w sieci RS485 jedynie poprzez zdefiniowane wcześniej zapytania i ignoruje zapytania adresowane do innych urządzeń w sieci Modbus TCP. Komunikacja z zewnętrznymi modułami odbywa się wyłącznie poprzez wewnętrzne rejestry modułu z adresów1000 - 1099.

### Brama Modbus TCP

Moduł MO-ET-SL w trybie bramy TCP obsługuje maksymalnie do czterech klientów jednocześnie. Po połączeniu do modułu na odpowiednim porcie moduł oczekuje na ramki zgodne ze specyfikacją Modbus TCP. W pierwszej kolejności po odebraniu jakiegokolwiek pakietu na tym porcie urządzenie sprawdza jego poprawność. Jeśli długość pakietu będzie niepoprawna urządzenie odeśle błąd z kodem Modbus0x03 – Illegal Data Value. Jeśli zapytanie jest poprawne i adresowane do modułuMOD-ETH, to wykonywana jest funkcja z zapytania. Jeśli nie jest to funkcja Modbus, to urządzenie również zwróci błąd z kodem 0x04 – Server Device Failure.Po przetworzeniu zapytania i przygotowaniu odpowiedzi moduł odsyła ją zgodnie ze specyfikacją protokołu Modbus TCP.

Jeśli zapytanie nie jest adresowane do modułu MO-ET-SL oraz jest ustawiony tryb bramy TCP, to urządzenie konwertuje zapytanie na Modbus RTU/ASCII i wysyła je po magistrali RS485. Wówczas MO-ET-SL czeka wyznaczony czas na odpowiedźi blokuje dostęp do magistrali RS485 dla innych klientów, aby nie nastąpił konflikt pakietów. Jeśli moduł odbierze odpowiedź lub minie timeout, to magistrala zostaje zwolniona i w przypadku odebrania pakietu na szynie RS485 jest on sprawdzany pod kątem zgodności z trybem RTU lub ASCII. W przypadku poprawnego pakietu jest on konwertowany na Modbus TCP i wysyłany do klienta. W przypadku błędu wysyłany jest kod 0x04 jeśli odpowiedź nie została odebrana w wyznaczonym czasie lub kod 0x03, jeśli pakiet jest niepoprawny. Natomiast jeśli moduł nie otrzyma dostępu do sieci RS485 zwrócony zostanie pakiet z błędem 0x06 – Server Device Busy.

### Device Table

Podczas działania w trybie Device Table klient łączy się z urządzeniem tak samo jak w przypadku trybu bramy. Istotną różnicą jest natomiast, że moduł MO-ET-SL będzie ignorował wszystkie zapytania nie adresowane do niego. Komunikacja z zewnętrznymi urządzeniami następuje jedynie poprzez konfigurację zdalnych zapytań poprzez stronę www (szczegóły w - Device Table) i odczyt/zapis z/do wewnętrznych rejestrów modułu MO-ET-SL. Każde zapytanie jest zapamiętywane automatycznie do pamięci nieulotnej modułu.

Jeśli konfiguracja zapytania jest poprawna, to urządzenie w tym trybie na bieżąco odpytuje moduły poprzez zapisane zapytania i zapisuje odpowiedzi w wewnętrznych rejestrach wskazanych przez użytkownika w przypadku czytania danych lub pobiera dane z tych rejestrów w przypadku ich zapisu. W przypadku nieprawidłowo skonfigurowanego zapytania lub braku odpowiedzi od urządzenia pytanego odpowiedni komunikat zostanie wyświetlony na stronie w zakładce Devices, kolumnie Status.

