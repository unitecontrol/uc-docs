# Unite Control system and devices documentation #

You can find instructions for specific devices and general description of UC Cloud system here.

* System — general concepts and API
* Devices — modules from UC

[Latest documentation is available in repository](http://bitbucket.com/unitecontrol/uc-docs)