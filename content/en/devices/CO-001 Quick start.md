# Quick start guide

1. Connect WiFi antenna to the left SMA connector. 
2. Connect power adapter to the terminal blocks Vcc and GND. UC HUB supports 10...30 VDC, 20Wt minimum.
3. UC HUB boots Linux. You should see orange LED Power. If this doesn't happen, you need to press Power button. Boot time is up to 1 minute on average. After successful boot will see LEDs: Power, Internet and GSM.
4. Connect to the LAN: connect UC HUB with your router with Ethernet cable. Router has to have working DHCP server.
5. To turn off UC HUB, you have to hold Power button for at least 5 seconds. LED will change color to orange. UC HUB will turn off all processes safely. This usually takes about 1 minute.

## LEDs

### Power

Status | Meaning
--- | ---
Orange lights | Hub is loading
Green lights | Power OK Battery OK
Green blinks | Power OK Battery not connecting
Red lights | NO Power Battery ON
Red blinks | NO Power Battery LOW

### Internet

Status | Meaning
--- | ---
Green lights | Internet OK WiFi AP OFF
Green blinks | Internet OK WiFi AP ON
Red blinks | Internet NO WiFi AP ON
Red lights | Internet NO WiFi AP OFF	


### GSM

Status | Meaning
--- | ---
Green lights | Signal OK
Green blinks | Modem has initialized, but GSM signal is low
Red lights | Initialization error or network is unavailable
No lights | Modems are not connected


## Buttons

Button | Push | Explanation
--- | --- | ---
Power | Short click | Power ON
Power | Long push (> 2 sec) | Power OFF
WiFi | Long push (> 5 sec) | ON/OFF WiFi AP
P | — | —

# Linux Guide
## Initial connection

1. Connection using WiFi: hold WiFi button for at least 5 seconds. LED "Internet" will blinks (green or red). Connect to WiFi network (ssid: ***UC-HUB***, password: ***unitecontrol***)
2. Connect to UC HUB:
  * with SSH (22 port): #ssh ***root@10.10.10.1***, default password: ***unitecontrol*** (Use Putty for Windows http://www.putty.org)
  * with SFTP (22 порт), user: ***root***, default password: ***unitecontrol*** (Use FileZilla https://filezilla-project.org/)
3. Ethernet/WiFi settings change via WEB interface ***http://10.0.0.1*** from UC-Hub WiFi network.
4.  Linux version - Debian 8. Log and boot system - systemd. [Getting started with systemd](https://medium.com/@johannes_gehrs/getting-started-with-systemd-on-debian-jessie-e024758ca63d)

## Change root password

| *root@UC:~# passwd* |

#### Date and Time

| *root@UC:~# timedatectl* |
|       *Local time: Mon 2015-08-24 11:23:27 UTC* |
|     *Universal time: Mon 2015-08-24 11:23:27 UTC* |
|           *RTC time: Mon 2015-08-24 07:22:21* |
|          *Time zone: Etc/UTC (UTC, +0000)* |
|        *NTP enabled: yes* |
|   *NTP synchronized: yes* |
|    *RTC in local TZ: no* |
|         *DST active: n/a* |

#### Set time and date

| *root@UC:~# timedatectl set-time "yyyy-MM-dd hh:mm:ss"* |

#### Select time zone
*root@UC:~# timedatectl list-timezones*
| *Africa/Abidjan* |
| *Africa/Accra* |
| *Africa/Addis_Ababa* |
| *Africa/Algiers* |
| *Africa/Asmara* |
| *...* |

*root@UC:~# timedatectl set-timezone Zone/SubZone*


## Interfaces

Physical | In linux
--- | ---
1-wire | /dev/i2c-2
RS485-1 | /dev/serial/rs485-1
RS485-2 | /dev/serial/rs485-2
RS232 | /dev/serial/rs232
Modem | /dev/ttyS2 

## 1-wire

OWFS is installed and configured. Check /etc/owfs.conf
Service owserver is run by default. 


## Scheme connections
#### Connect power supply

![](images/CO-001/connection1.png) {.img-responsive}

#### Connect RS485 devices

![](images/CO-001/connection2.png) {.img-responsive}

#### Connect RS232 devices

![](images/CO-001/connection3.png) {.img-responsive}

##### Connect MBus devices

![](images/CO-001/connection4.png) {.img-responsive}

#### Connect UC-Link (1-wire) devices

![](images/CO-001/connection5.png) {.img-responsive}

<br>*UL-TE - temperature sensor 1-Wire DS18B20



