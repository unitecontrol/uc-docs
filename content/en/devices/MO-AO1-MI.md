# UC MO-AO1-MI {.title}

[UC MO-AO1-MI Expansion Module](http://en.unitecontrol.com/MO-AO1-MI.html), 1 analog output, 2 digital inputs Modbus RTU (RS485)

![](images/MO-AO1-MI/device.png) {.img-responsive}

## User Manual

## Safety Rules

* Before first use, refer to this manual
* Before first use, make sure that all cables are connected properly
* Please ensure proper working conditions, according to the device specifications (e.g. supply voltage, temperature, maximum power consumption)
* Before making any modifications to wiring connections, turn off the power supply


## Module Features

### Purpose and description of the module

- Name: Expansion Module, 1 analog output, Modbus RTU (RS485), ASCII
- art. MO-AO1-MI
- Module MO-AO1 works in Slave mode.
- Module MO-AO1 has 1 analog current output (0-20 mA, 4-20 mA) and 1 voltage output (0-10 V). 
- In addition, the module is equipped with 2 configurable digital inputs with counter function and ability to connect encoder.
- This module is connected to the RS485 bus with twisted-pair wire. Communication is via Modbus RTU or Modbus ASCII.
- The module is designed for mounting on a DIN rail in accordance with DIN EN 5002.
- The module is equipped with a set of LEDs are used to indicate the status of inputs and outputs useful for diagnostics purposes and helping to find errors.
- Module configuration is done via USB by using a dedicated computer program. You can also change the parameters using the ModBus protocol.

### Techical Specifications

<table>
  <tr>
      <td>
          Power Supply
      </td>
      <td>
          Voltage
      </td>
      <td>
          10-36V DC
		  <br>10-28V AC
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Maximum Current
      </td>
      <td>
          DC: 90 mA @ 24V
		  <br>AC: 170 mA @ 24V
      </td>
  </tr>
  <tr>
      <td>
          Connection Speed
      </td>
      <td>
      </td>
      <td>
          2400 - 115200 bps
      </td>
  </tr>
  <tr>
      <td>
          Outputs
    </td>
    <td>
      No of outputs
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Voltage output
    </td>
    <td>
      0V do 10V (resolution 1.5mV)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Current output
    </td>
    <td>
      0mA do 20mA (resolution 5 μA)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Current output
    </td>
    <td>
      4mA do 20mA (value in ‰ – 1000 steps) (resolution 16 μA)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Measurement resolution
    </td>
    <td>
      12 bits
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      ADC processing time
    </td>
    <td>
      16ms / channel
    </td>
  </tr>
  <tr>
      <td>
          Digital Inputs
    </td>
    <td>
      No of inputs
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Voltage range
    </td>
    <td>
      0 – 36 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Low State „0”
    </td>
    <td>
      0 – 3 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      High State „1”
    </td>
    <td>
      6 – 36 V
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Input impedance
    </td>
    <td>
      4 кΩ
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Isolation
    </td>
    <td>
      1500 Vrms
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Input Type
    </td>
    <td>
      PNP or NPN
    </td>
  </tr>
  <tr>
    <td>
      Counters
    </td>
    <td>
      No
    </td>
    <td>
      2
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Resolution
    </td>
    <td>
      32 bits
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Frequency
    </td>
    <td>
      1kHz (max)
    </td>
  </tr>
  <tr>
    <td>
    </td>
    <td>
      Impulse Width
    </td>
    <td>
      500 μs (min)
    </td>
  </tr>
  <tr>
      <td>
          Temperature
      </td>
      <td>
          Work
      </td>
      <td>
          -20°C ÷ +65°C
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Storage
      </td>
      <td>
          -40°C ÷ +85°C
      </td>
  </tr>
  <tr>
      <td>
          Connectors
      </td>
      <td>
          Power Supply
      </td>
      <td>
          2 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Communication
      </td>
      <td>
          3 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Inputs &amp; Outputs
      </td>
      <td>
          2 x 3 pin
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Configuration
      </td>
      <td>
          MiniUSB
      </td>
  </tr>
  <tr>
      <td>
          Dimensions
      </td>
      <td>
          Height
      </td>
      <td>
          90 mm
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Length
      </td>
      <td>
          56 mm
      </td>
  </tr>
  <tr>
      <td>
      </td>
      <td>
          Width
      </td>
      <td>
          17 mm
      </td>
  </tr>
  <tr>
      <td>
          Interface
      </td>
      <td>
          RS485
      </td>
      <td>
          Up to 128 devices
      </td>
  </tr>
</table>

### Dimensions of the product

Look at dimensions of the module are shown below. The module is mounted directly to the rail in the DIN industry standart.

![](images/MO-AO1-MI/overview.png) {.img-responsive}

## Communication configuration

### Grounding and shielding

In most cases, IO (Inputs/Outputs) modules will be installed in an enclosure along with other devices which generate electromagnetic radiation. Examples of these devices are relays and contactors, transformers, motor controllers etc. This electromagnetic radiation can induce electrical noise into both power and signal lines, as well as direct radiation into the module causing negative effects on the system. Appropriate grounding, shielding and other protective steps should be taken at the installation stage to prevent these effects. These protective steps include control cabinet grounding, module grounding, cable shield grounding, protective elements for electromagnetic switching devices, correct wiring as well as consideration of cable types and their cross sections.

### Network Termination

Transmission line effects often present a problem on data communication networks. These problems include reflections and signal attenuation.

To eliminate the presence of reflections from the end of the cable, the cable must be terminated at both ends with a resistor across the line equal to its characteristic impedance. Both ends must be terminated since the direction of propagation is bidirectional. In the case of an RS485 twisted pair cable this termination is typically 120 Ω.

### Types of Modbus Registers

There are 4 types of variables available in the module

<table>
  <tr>
      <th>
          <b>Type</b>
      </th>
      <th>
          <b>Beginning address</b>
      </th>
      <th>
          <b>Variable</b>
      </th>
      <th>
          <b>Access</b>
      </th>
      <th>
          <b>Modbus Command</b>
      </th>
  </tr>
  <tr>
      <td>
          1
      </td>
      <td>
          00001
      </td>
      <td>
          Coils
      </td>
      <td>
          Read &amp; Write
      </td>
      <td>
          1, 5, 15
      </td>
  </tr>
  <tr>
      <td>
          2
      </td>
      <td>
          10001
      </td>
      <td>
          Discrete Inputs
      </td>
      <td>
          Read
      </td>
      <td>
          2
      </td>
  </tr>
  <tr>
      <td>
          3
      </td>
      <td>
          30001
      </td>
      <td>
          Input Registers
      </td>
      <td>
          Registered Read
      </td>
      <td>
          3
      </td>
  </tr>
  <tr>
      <td>
          4
      </td>
      <td>
          40001
      </td>
      <td>
          Holding Registers
      </td>
      <td>
          Read & Write
      </td>
      <td>
          4, 6, 16
      </td>
  </tr>
</table>

### Communication settings

The data stored in the modules memory are in 16-bit registers. Access to registers is via MODBUS RTU or MODBUS ASCII.

#### Default settings

<table>
  <tr>
      <td>
          Baud rate, bps
      </td>
      <td>
          19200
      </td>
  </tr>
  <tr>
      <td>
          Adress
      </td>
      <td>
          1
      </td>
  </tr>
  <tr>
      <td>
          Parity
      </td>
      <td>
          No
      </td>
  </tr>
  <tr>
      <td>
          Data bits
      </td>
      <td>
          8
      </td>
  </tr>
  <tr>
      <td>
          Stop bits
      </td>
      <td>
          1
      </td>
  </tr>
  <tr>
      <td>
          Reply Delay, ms
      </td>
      <td>
          0
      </td>
  </tr>
  <tr>
      <td>
          Modbus Type
      </td>
      <td>
          RTU
      </td>
  </tr>
</table>

#### Configuration registers

<table>
  <thead>
      <tr>
          <th>
              Address
          </th>
          <th>
              Name
          </th>
          <th>
              Values
          </th>
      </tr>
  </thead>
  <tbody>
      <tr>
          <td>
              40002
          </td>
          <td>
              Adress
          </td>
          <td>
              From 0 to 255
          </td>
      </tr>
	  <tr>
          <td>
              40003
          </td>
          <td>
              Baud rate
          </td>
          <td>
              0 - 2400
              <br>1 - 4800
              <br>2 - 9600
              <br>3 - 19200
              <br>4 - 38400
              <br>5 - 57600
              <br>6 - 115200
              <br>other – value * 10
          </td>
      </tr>
      <tr>
          <td>
              40005
          </td>
          <td>
              Parity
          </td>
          <td>
              0 - none
              <br>1 - odd
              <br>2 - even
              <br>3 - always 1
              <br>4 - вalways 0
          </td>
      </tr>
      <tr>
          <td>
              40004
          </td>
          <td>
              Stop Bits LSB
          </td>
          <td>
              1 - one stop bit
              <br>2 - two stop bits
          </td>
      </tr>
      <tr>
          <td>
              40004
          </td>
          <td>
              Data Bits MSB
          </td>
          <td>
              7 - 7 data bits
              <br>8 - 8 data bits
          </td>
      </tr>
      <tr>
          <td>
              40006
          </td>
          <td>
              Response delay
          </td>
          <td>
              Time in ms
          </td>
      </tr>
      <tr>
          <td>
              40007
          </td>
          <td>
              Modbus Mode
          </td>
          <td>
              0 - RTU
              <br>1 - ASCII
          </td>
      </tr>
  </tbody>
</table>

## Indicators

![](images/MO-AO1-MI/leds.gif) {.img-responsive}

<table>
  <tr>
      <th>
          Indicator
      </th>
      <th>
          Description
      </th>
  </tr>
  <tr>
      <td>
          Power (ON)
      </td>
      <td>
          LED indicates that the module is correctly powered.
      </td>
  </tr>
  <tr>
      <td>
          TX
      </td>
      <td>
          The LED lights up when the unit received the correct packet and sends the answer.
      </td>
  </tr>
  <tr>
      <td>
          AI<sub>i</sub>, AI<sub>v</sub>
      </td>
      <td>
          LED indicates that the signal to input is connected and is different from 0.
      </td>
  </tr>
  <tr>
      <td>
          DO
      </td>
      <td>
          LED indicates that the output is on.
      </td>
  </tr>
</table>

## Block diagram

![](images/MO-AO1-MI/scheme.png) {.img-responsive}

## Module Connection

![](images/MO-AO1-MI/connection.gif) {.img-responsive}

## Modules Registers

### Registered access

<table>
  <thead>
    <tr>
      <th>
        Address
      </th>
      <th>
        Dec
      </th>
      <th>
        Hex
      </th>
      <th>
        <b>Register name</b>
      </th>
      <th>
        Acess
      </th>
      <th>
        Description
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        30001
      </td>
      <td>
        0
      </td>
      <td>
        0x00
      </td>
      <td>
        Version/Type
      </td>
      <td>
        Read
      </td>
      <td>
        Version and Type of the device
      </td>
    </tr>
    <tr>
      <td>
        40002
      </td>
      <td>
        1
      </td>
      <td>
        0x01
      </td>
      <td>
        Address
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Module Address
      </td>
    </tr>
    <tr>
      <td>
        40003
      </td>
      <td>
        2
      </td>
      <td>
        0x02
      </td>
      <td>
        Baud rate
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        RS485 baud rate
      </td>
    </tr>
    <tr>
      <td>
        40004
      </td>
      <td>
        3
      </td>
      <td>
        0x03
      </td>
      <td>
        Stop Bits & Data Bits
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of Stop bits &amp; Data Bits
      </td>
    </tr>
    <tr>
      <td>
        40005
      </td>
      <td>
        4
      </td>
      <td>
        0x04
      </td>
      <td>
        Parity
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Parity bit
      </td>
    </tr>
    <tr>
      <td>
        40006
      </td>
      <td>
        5
      </td>
      <td>
        0x05
      </td>
      <td>
        Response Delay
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Response delay in ms
      </td>
    </tr>
    <tr>
      <td>
        40007
      </td>
      <td>
        6
      </td>
      <td>
        0x06
      </td>
      <td>
        Modbus Mode
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Modbus Mode (ASCII or RTU)
      </td>
    </tr>
    <tr>
      <td>
        40009
      </td>
      <td>
        8
      </td>
      <td>
        0x08
      </td>
      <td>
        Watchdog
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Watchdog
      </td>
    </tr>
    <tr>
      <td>
        40033
      </td>
      <td>
        32
      </td>
      <td>
        0x20
      </td>
      <td>
        Received packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40034
      </td>
      <td>
        33
      </td>
      <td>
        0x21
      </td>
      <td>
        Received packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets
      </td>
    </tr>
    <tr>
      <td>
        40035
      </td>
      <td>
        34
      </td>
      <td>
        0x22
      </td>
      <td>
        Incorrect packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40036
      </td>
      <td>
        35
      </td>
      <td>
        0x23
      </td>
      <td>
        Incorrect packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of received packets with error
      </td>
    </tr>
    <tr>
      <td>
        40037
      </td>
      <td>
        36
      </td>
      <td>
        0x24
      </td>
      <td>
        Sent packets LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        40038
      </td>
      <td>
        37
      </td>
      <td>
        0x25
      </td>
      <td>
        Sent packets MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        No of sent packets
      </td>
    </tr>
    <tr>
      <td>
        30051
      </td>
      <td>
        50
      </td>
      <td>
        0x32
      </td>
      <td>
        Inputs
      </td>
      <td>
        Read
      </td>
      <td>
        Inputs state
      </td>
    </tr>
	<tr>
      <td>
        30052
      </td>
      <td>
        51
      </td>
      <td>
        0x33
      </td>
      <td>
        Outputs
      </td>
      <td>
        Read
      </td>
      <td>
        Outputs state
      </td>
    </tr>
    <tr>
      <td>
        40053
      </td>
      <td>
        52
      </td>
      <td>
        0x34
      </td>
      <td>
        Current output
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Value of analog output: in μA for:  
		<br>0 - 20mA (max 20480);
        <br>in ‰ for: 
		<br>4-20mA (max 1000)
      </td>
    </tr>
    <tr>
      <td>
        40054
      </td>
      <td>
        53
      </td>
      <td>
        0x35
      </td>
      <td>
        Voltage output
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        in mV (max 10240);
      </td>
    </tr>
    <tr>
      <td>
        40055
      </td>
      <td>
        54
      </td>
      <td>
        0x36
      </td>
      <td>
        Counter 1 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 1
      </td>
    </tr>
    <tr>
      <td>
        40056
      </td>
      <td>
        55
      </td>
      <td>
        0x37
      </td>
      <td>
        Counter 1 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 1
      </td>
    </tr>
    <tr>
      <td>
        40057
      </td>
      <td>
        56
      </td>
      <td>
        0x38
      </td>
      <td>
        Counter 2 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 2
      </td>
    </tr>
    <tr>
      <td>
        40058
      </td>
      <td>
        57
      </td>
      <td>
        0x39
      </td>
      <td>
        Counter 2 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit counter 2
      </td>
    </tr>
    <tr>
      <td>
        40059
      </td>
      <td>
        58
      </td>
      <td>
        0x3A
      </td>
      <td>
        CCounter 1 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 1
      </td>
    </tr>
    <tr>
      <td>
        40060
      </td>
      <td>
        59
      </td>
      <td>
        0x3B
      </td>
      <td>
        CCounter 1 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 1
      </td>
    </tr>
    <tr>
      <td>
        40061
      </td>
      <td>
        60
      </td>
      <td>
        0x3C
      </td>
      <td>
        CCounter 2 LSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 2
      </td>
    </tr>
    <tr>
      <td>
        40062
      </td>
      <td>
        61
      </td>
      <td>
        0x3D
      </td>
      <td>
        CCounter 2 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 2
      </td>
    </tr>
    <tr>
      <td>
        40063
      </td>
      <td>
        62
      </td>
      <td>
        0x3E
      </td>
      <td>
        CCounter 2 MSB
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        32-bit value of captured counter 2
      </td>
    </tr>
    <tr>
      <td>
        40064
      </td>
      <td>
        63
      </td>
      <td>
        0x3F
      </td>
      <td>
        Status
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Captured counter
      </td>
    </tr>
    <tr>
      <td>
        40065
      </td>
      <td>
        64
      </td>
      <td>
        0x40
      </td>
      <td>
        Default current output value
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Default value of output set when power is on or when watchdog reset occurs
      </td>
    </tr>
    <tr>
      <td>
        40066
      </td>
      <td>
        65
      </td>
      <td>
        0x41
      </td>
      <td>
        Default voltage output value
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Default value of output set when power is on or when watchdog reset occurs
      </td>
    </tr>
    <tr>
      <td>
        40067
      </td>
      <td>
        66
      </td>
      <td>
        0x42
      </td>
      <td>
        Current output settings
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Setting of output mode:
        <br>0 — output disabled;
        <br>2 — current output 0-20mA;
        <br>3 — current output 4-20mA;
      </td>
    </tr>
    <tr>
      <td>
        40068
      </td>
      <td>
        67
      </td>
      <td>
        0x43
      </td>
      <td>
        Voltage output settings
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        0 — output disabled;
		<br>1 — voltage output;
      </td>
    </tr>
    <tr>
      <td>
        40069
      </td>
      <td>
        68
      </td>
      <td>
        0x44
      </td>
      <td>
        Counter Config 1
      </td>
      <td>
        Read &amp; Write
      </td>
      <td rowspan="2">
        Counter Configuration:
        <br>+1 – time measurement (if 0 is counting impulses)
        <br>+2 – autocatch counter every 1 sec
        <br>+4 – catch value when input is low
        <br>+8 – reset counter after catch
        <br>+16 – reset counter if input low
        <br>+32 – encoder
       </td>
    </tr>
    <tr>
      <td>
        40070
      </td>
      <td>
        69
      </td>
      <td>
        0x45
      </td>
      <td>
        Counter Config 2
      </td>
      <td>
        Read &amp; Write
      </td>
    </tr>
   </tbody>
</table>

### Bit access

<table>
  <thead>
    <tr>
      <th>
        Modbus Address
      </th>
      <th>
        Dec
      </th>
      <th>
        Hex
      </th>
      <th>
        <b>Register name</b>
      </th>
      <th>
        Access
      </th>
      <th>
        Description
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        801
      </td>
      <td>
        800
      </td>
      <td>
        0x320
      </td>
      <td>
        Input 1
      </td>
      <td>
        Read
      </td>
      <td>
        Input 1 state
      </td>
    </tr>
    <tr>
      <td>
        802
      </td>
      <td>
        801
      </td>
      <td>
        0x321
      </td>
      <td>
        Input 2
      </td>
      <td>
        Read
      </td>
      <td>
        Input 2 state
      </td>
    </tr>
    <tr>
      <td>
        817
      </td>
      <td>
        816
      </td>
      <td>
        0x330
      </td>
      <td>
        Output 1
      </td>
      <td>
        Read
      </td>
      <td>
        If current output is greater than 0 then according bit is set.
      </td>
    </tr>
    <tr>
      <td>
        818
      </td>
      <td>
        817
      </td>
      <td>
        0x331
      </td>
      <td>
        Output 2
      </td>
      <td>
        Read
      </td>
      <td>
        If voltage output is greater than 0 then according bit is set.
      </td>
    </tr>
    <tr>
      <td>
        993
      </td>
      <td>
        992
      </td>
      <td>
        0x3E0
      </td>
      <td>
        Capture 1
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 1
      </td>
    </tr>
    <tr>
      <td>
        994
      </td>
      <td>
        993
      </td>
      <td>
        0x3E1
      </td>
      <td>
        Capture 2
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Capture counter 2
      </td>
    </tr>
    <tr>
      <td>
        1009
      </td>
      <td>
        1008
      </td>
      <td>
        0x3F0
      </td>
      <td>
        Captured 1
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Counter captured 1
      </td>
    </tr>
    <tr>
      <td>
        1010
      </td>
      <td>
        1009
      </td>
      <td>
        0x3F1
      </td>
      <td>
        Captured 2
      </td>
      <td>
        Read &amp; Write
      </td>
      <td>
        Counter captured 2
      </td>
    </tr>
   </tbody>
</table>

## Configuration software

Modbus Configurator is software that is designed to set the module registers responsible for communication over Modbus network as well as to read and write the current value of other registers of the module. This program can be a convenient way to test the system as well as to observe real-time changes in the registers.
Communication with the module is done via the USB cable. The module does not require any drivers.

Configurator is a universal program, whereby it is possible to configure all available modules.

![](images/MO-AO1-MI/configurator.png) {.img-responsive}