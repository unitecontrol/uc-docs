![](images/spda.png) {.img-thumbnail}

* Driver select - выбор драйвера устройства
* Communication settings - ввод настроек для подключения. Можно выделить 2 особенных случая - это использование serial port (для всех драйверов с параметром serial).
* Device discovery - автопоиск устройств. Только драйвера с флагом  autodiscovery
* Other settings - установка настроек или правка настроек после автопоиска.
* Variables - настройка переменных или правка переменных после автопоиска.
* Converter - настройка конвертации для каждой переменной
* Logger - настройка логирования для каждой переменной

Модели и реализация:

* [Driver](driver.html)
* [Device](device.html)
* [Serial port](serial_port.html)