## Hub

Хранит настройки хаба и токен для доступа к API.

### Модель

``` Javascript
{
    // id хаба
    id: String,
    // токен для авторизации в api
    token: String,
    // серийный нoмер хаба
    serial: {type: Number, min: 100000, max: 999999},
    // флаги указывающие на необходимость перезагрузки хаба
    restart: {
        //перезагрузка UC сервера
        server: {type: Boolean, default: false},
        //полная перезагрузка хаба
        hub: {type: Boolean, default: false}
    },
    // имя хаба
    name: String,
    // описание хаба
    description: String,
    // настройки хаба
    settings: {
        // флаг указывающий на то, что настройки хаба были изменены
        changed: {type: Boolean, default: false},
        // время изменения настроек
        ts: {type: Date},
        // настройки сети
        network: {
            ethernet: {
                enabled:{
                    type: Boolean,
                    default: true
                },
            dhcp: {
                type: Boolean,
                default: true
            },
            ip: String,
                netmask: String,
                network: String,
                broadcast: String,
                dns: String,
                gateway: String
            },
            wifi: {
                enabled:{ type: Boolean, default: true},
                ssid: String,
                password: String,
                security: {
                    type: String,
                    enum: ['wpa', 'wep', 'open']
                },
                dhcp: {
                    type: Boolean,
                    default: true
                },
                ip: String,
                netmask: String,
                network: String,
                broadcast: String,
                dns: String,
                gateway: String
            },
            gsm: {
                enabled:{
                    type: Boolean,
                    default: true
                },
                apn: String,
                login: String,
                password: String,
                ip: String,
                pin: String
            }
        }
    },
    // тип хаба ['none','consumption']
    type: String,
    // привязан к пользователю
    linked: {type: Boolean, default: false},
    // токен был использован для авторизации
    authorized: {type: Boolean, default: false}
}
```

## Variable

Хранит состояния устройства или системы. Используется в разлиных скриптах, при запросе текущих данных и т.д.

### Модель

``` Javascript
{
    // id хаба
    hubid:String,
    // id переменной (кэп)
    id:String,
    // имя переменной
    name: String,
    // тип логируемой переменной
    type: {type: String, enum: ['cumulativenumber', 'number', 'binary'], required: true, default:'number'},
    // текущее состояние (значение)
    value:Number,
    // флаг доступности на прямую запись
    readonly:Boolean
}
```

**type**
* cumulativenumber - накапливаемые значения
* binary - бинарные значения 0/1, интерпритирует любое значение в 0/1 (false - 0, true - 1)
* number - стандартные значения с датчиков
* discrete - дискретные значения `Version 2.0`

## Converter

Конвертирует значения переменной, создает новую переменную

### Модель

``` Javascript
{
    // id хаба
    hubid: String,
    // путь до переменной
    variableid: String,
    // имя конвертера
    name: String,
    // id конвертера
    id: String,
    // настройки конвертации, возможна только одна опция
    convert: {
        // код конвертера на JS. Выполняется в через contextify
        code:String,
        // простое умножение на число
        multiply:Number
    },
    // Тип логируемого канала
    type: {type: String, enum: ['cumulativenumber', 'number', 'binary'], required: true, default:'number'},
    // текущее состояние (значение)
    value: Number,
    // конвертер активен
    active: Boolean
}
```

**Пример кода конвертера**
``` Javascript
output = input * 2;
```

## Tariff

Конвертирует значения при запросе логов или текущего состояния. Уникален для каждой переменной.

### Модель

``` Javascript
{
    // id хаба
    hubid:String,
    // путь до переменной, уникально
    variableid: String,
    // стоимость еденицы
    cost: Number,
    // валюта
    currency:String,
    // тариф активен
    active:Boolean
}
```

## User Interface

**ui** - плитка в интерфейсе.

### Модель

``` Javascript
{
    // id хаба
    hubid:String,
    // уникальный ID для каждой плитки
    id:String,
    // индификатор пользователя
    userid: String,
    // имя
    name: String,
    // родительская плитка
    parent: String,
    // номер для сортировки
    sort: Number,
    // тип ['folder', 'channel']
    type: String,
    // иконка
    icon: String,
    // описание
    description: String,
    // контрол
    control: String,
    // еденицы исчисления
    units: String,
    // перменные и настройки
    settings:[{
        variableid:String
        loggerid:String
        ui:String
    }]
}
```
