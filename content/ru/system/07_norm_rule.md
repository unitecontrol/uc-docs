**Правило нормы** - отправка уведомления если один из показателей вышел из нормы (не удволетворяет условиям)

### Модель

``` Javascript
{
  id:{type:String, default:getShortId},
  hubid:{type:String, required:true, index:true},
  // id системной переменной для отслеживания
  variableid:{type:String, required:true},
  // правило активно
  active:{type:Boolean, default:true},
  // id ui с которого происходит настройка правила
  ui:{type:String},
  // условие
  rule:{
    // Оператор сравнения
    operator:{
      type:String,
      enum:['eq','gt','gte','lt','lte','ne','bt'],
      default:'eq',
      required:true
    },
    // Количество аргументов
    args:{type:[Number], required:true},
    // Текущее состояние нормы
    status:{
      type:Boolean,
      default:false,
      required:true
    }
  },
  // Уведомление
  notification: {
    // Условия срабатывания при изменении rule.status
    trigger: {
      type: String,
      required: true,
      enum: ['out','in','both'],
      default:'out'
    },
    sms: {
      active:{type:Boolean, default:false}, phones:[String]
    },
    email:{
      active:{type:Boolean, default:false}, addresses:[String]
    }
  }
}
```

### Условие

**Оператор и кол-во аргументов**
* eq - равно, `1`
* gt - больше, `1`
* gte - больше или равно, `1`
* lt - меньше, `1`
* lte - меньше или равно, `1`
* ne - не равно, `1`
* bt - между, `2`
* bte - между или равно, `2`

#### Действие

* sms - отправка sms на номера из списка
* email - отправка email на адреса из списка
* app - отправка уведомления в аппе
