**Script** - запуск подпрограмм по времени, интервалу или при изменении связанных переменных.

Включает в себя:
* Условия срабатывания
* Подпрограмму
* Связанные переменные

## Основные свойства и компоненты

### Связанные переменные

Массив переменных, которые будут использоваться в сценарии.

### Условия срабатывания

* По графику. Формат cron (https://www.npmjs.com/package/cron-parser). Максимум на один хаб может быть **50** скриптов с запуском по графику.
* Дата и время
* Изменение связанных переменных


#### Модель

``` Javascript
{
    // id хаба
    hubid:String,
    // подпрограмма
    code: String,
    // валюта
    variables:[{
        //id системной перменной или конвертера
        id: String,
        //имя внутри подпрограммы
        name: String
    }],
    // активация скрипта
    condition: {
        // по конкретному времени
        date: String,
        // по графику
        schedule: String,
        // при изменении связанных переменных
        data: Boolean
    },
}
```

### Подпрограмма

Написанная на Javascript программа. Связанные переменные доступны через массив или по ключу, указонному в массиве связанных переменных.

#### Примеры:

**Регулировка температуры**
```javascript
if(var0 > var1 - 0.1) {
    var3 = 1;
} else {
    if(var0 < var1 + 0.1) {
        var2 = 0;
    }
}
```

где ```var0```, ```var1```, ```var2``` - имена перeменных и/или конвертеров, связанных с реальными
перменными/конвертерами через свойство модели ```variables```