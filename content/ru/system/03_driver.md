## Driver

Предоставляет единый интерфейс для настройки различных драйверов и связывания их с переменными в системе.
А так-же может содержать в себе сам код для работы по различным протоколам.
Сами настройки сохраняются в [Device](device.html)

### Модель

``` Javascript
{
  // ID драйвера
  id: {type: String, required: true},
  // ID хаба, any - для встроенных драйверов.
  hubid: {type: String, index: true, required: true, default: 'any'},
  // Имя драйвера для показа в интерфейсе
  title: {type: String},
  // Описание драйвера
  description: {type: String},
  // Код драйвера, отсутсвует для встроенных драйверов.
  code: {type: String},
  // Список настроек необходиых для иницилизации поиска устройств
  autodiscover:[{type:String}],
  // Шаблоны для интерфейса настройки
  templates: {
      settings: {type: Schema.Types.Mixed, required: true},
      channels: [{
        title: {type: String, required: true},
        unique: {type: Boolean},
        settings:{type: Schema.Types.Mixed, required: true}
      }]
  }
}
```

**templates**
* discover - настройки формы для ввода необходимых настроек для автопоиска.
* settings - настройки формы для ввода настроек драйвера.
* channels
* * title - название шаблона переменной
* * unique - уникальная переменная для устройства
* * settings - настройки формы для ввода настроек переменной (см. выше)

#### Настройки формы ввода значений

*Примеры настройки можно посмотреть в [1-wire](drivers/1-wire.html)*

**Псведосхема**
``` Javscript
{
  title: {type: String, required: true},
  type: {type: String, enum: ['number','string'], required: true},
  enum: [{type: String}],
  match:{type:String},
  max: {type: Number},
  min: {type: Number},
  required: {type: Boolean},
  default: {type: [Number,String]},
  unique: {type: Boolean}
}
```

* key - имя настройки, как оно будет доступно в драйвере. Отображется, если не введен title.
* title - имя настройки для отображения
* type - тип вводимых данных
* enum - массив возможных значений для поля
* match - регулярное выражение для проверки поля (только для type == "string")
* max - масимальное значение числа или максимальная длина строки
* max - минимальное значение числа или минимальная длина строки
* required - обязательная настройка
* default - значение поля по-умолчанию

#### Шаблон драйвера

Общий шаблон для всех драйверов в системе (modbus, 1-wire, mbus и т.д.)

*Методы*

* create(device, serialport) - добавление устройства. Возвращает true/false.
* discover(serialport, callback) - поиск устройств. В callback(err, devices) - возвращается ошибка, либо массив Device.


