## Serial port

Пример старой реализации: [SPM](https://bitbucket.org/unitecontrol/uc-server/src/5bcf33bf32263016a5119e2fe6f6c692d4d3dc30/src/components/spm.js?at=spm#)


Работа с последовательными портами осуществляется через модуль [https://github.com/voodootikigod/node-serialport](https://github.com/voodootikigod/node-serialport)

Чтобы избежать конфликтов обращения к последовательным портам между драйверами, прямой управление ими невозможно. Serial port cоздает экземпляр последовательного порта и интерфейс для запросов в очереди (на картинке отмечен как SPM).

![](images/spm.png) {.img-responsive}


### Стандартный режим

Последовательный порт работает с любым устройством формируя очередь заданий.

*Основные виды коммуникаций:*

* Запрос - ответ (неколько овтетов). Переходить к следующему заданию после вызыва done в коллбэке для данных или по таймауту ответа.
* Передача всех ответов по коллбэку данных.

Стандартный таймаут ответа - 100мс
Стандартный таймаут done - 1000мс

*Методы:*

* write - добавляет задание в конец очередь.
* writeImmediate - добавляет задание в начало очереди.
* listen - передает в коллбэк все пришедшие данные на порт.
* add - добавляет задание в конец очередь.. После выполнения оно будет атоматически добавлено опять в конец очереди.

`TODO: delete,unlisten`

Примеры работы с очередью:

``` Javascript
serial.write(datаToSend,function(err,data,done){
  if(!err) {
    // do something with data
  }
  done();
});

var persistentData;
serial.write(datаToSend,function(err,data,done){
  if(!err) {
    if (data.toString('utf8').indexOf('\n') == -1) {
      persistentData += data;
    } else {
      // do something with persistentData
      persistentData = null;
      done();
    }
  } else {
    persistentData = null;
    done();
  }
})

serial.write(datаToSend,function(err,data,done){
  if(!err) {
    serial.writeImmediate(datаToSend,function(err,data,done){
      if(!err) {
        // do something with data
      }
      done();
    });
  }
  done();
});

serial.listen(function(data){
  // do something with data
});

serial.add(datаToSend,function(err,data,done){
  if(!err) {
    // do something with data
  }
  done();
});
```

### Эксклюзивный режим

Последовательный порт работает только с драйвером (устройствами созданными с помощью драйвера), id которого указано в свойстве exclusive. Предоставляет прямой доступ к порту.

### Модель

``` Javascript
{
  // id хаба
  hubid:{
    type:String,
    required:true
  },
  // уникальный ID для каждого порта в рамках одного хаба
  id:{
    type:String,
    required:true,
    enum:['rs485-1','rs485-2','rs232']
  },
  // описание порта
  description:{
    type:String
  },
  // Эксклюзивный режим использования. Указывается id драйвера.
  exclusive:{
    type:String
  },
  // настройки порта
  settings:{
    baudrate:{
      type:String,
      required:true,
      enum:[110,300,600,1200,2400,4800,9600,14400,19200,28800,38400,56000,57600,115200],
      default:115200
    },
    databits:{
      type:String,
      required:true,
      enum:['5','6','7','8'],
      default:'8'
    },
    stopbits:{
      type:String,
      required:true,
      enum:['1','1.5','2'],
      default:'1'
    },
    parity:{
      type:String,
      required:true,
      enum:['none','even','odd','mark','space'],
      default:'none'
    },
    // https://github.com/voodootikigod/node-serialport/blob/master/serialport.js#L143
    flowcontrol:{
      type:String,
      required:true,
      // auto - xany
      enum:['xon','xoff','auto','rcscts'],
      default:'auto'
    }
  },
  // Флаг активности
  active: {type: Boolean, default: false}
}
```