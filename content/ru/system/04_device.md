## Device

Отражает устройство из физического мира. Хранит настройки для создания экземпляра на основе драйвера и переменных.

### Модель

``` Javascript
{
    // id устрйоства
    id: {type: String, required: true, unique: true, 'default': getShortId},
    // id хаба
    hubid: {type: String, index: true, required: true},
    // имя устройства
    name: {type: String, required: true},
    // описание устройства
    description: {type: String},
    // ID драйвера
    driver: {type: String, required: true},
    // Настройки устройства
    settings: {type: Schema.Types.Mixed, required: true},
    // Настройки перменных
    channels: [{
        // Имя переменной
        name: {type: String, required: true, default: 'Unnamed'},
        // ID переменной
        variableid: String,
        // ID шаблона по которому она была создана
        templateid: {type: String, required: true},
        // Настройки переменной
        settings: {type: Schema.Types.Mixed, required: true}
    }]
    // ID последовательного порта
    serial: {type: String}
}
```