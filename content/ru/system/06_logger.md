## Logger

Определяет настройки логирования для конкретной переменной.

### Модель

``` JSON
{
  id: {type: String, required: true, unique: true},
  hubid: {type: String, required: true},
  // Id связанной переменной
  variableid: {type: String, required: true},
  // Событие для генерации лога
  trigger: {
    // Интервал в мс
    interval: {type: Number},
    // Изменение значение на опред. велечину
    deviation: {type: Number}
  },
    //Последнее значение привязанной переменной
  last: {
    value: {type: Number},
    ts: {type: Date}
  },
  active: {type: Boolean, required: true, default: true}
}
```

### Генерируемы логи

<table>
  <tr>
    <th></th>
    <th>Raw</th>
    <th>Hour</th>
    <th>Day</th>
    <th>Week</th>
    <th>Month</th>
    <th>Year</th>
  </tr>
  <tr>
    <td><b>Cumulative</b></td>
    <td>Raw data</td>
    <td>Sum for 5 min<br></td>
    <td>Sum for hour</td>
    <td>Sum for day</td>
    <td>Sum for day</td>
    <td>Sum for month</td>
  </tr>
  <tr>
    <td><b>Binary</b></td>
    <td>Raw data</td>
    <td>Raw data</td>
    <td>Timed avg for hour</td>
    <td>Timed avg for 6 hours</td>
    <td>Timed avg for day</td>
    <td>Timed avg for week</td>
  </tr>
  <tr>
    <td><b>Analog</b></td>
    <td>Raw data</td>
    <td>Raw data</td>
    <td>Approximation to<br>~100 points</td>
    <td>Approximation to<br>~100 points</td>
    <td>Approximation to<br>~100 points</td>
    <td>Approximation to<br>~100 points</td>
  </tr>
</table>

Если указан период (5 min, 6 hours) - ts должен быть равен началу периода (01-01-2015 00:00:00.000 - начало года и т.п.)

* Timed avg for ... - среднее значение по времени. По сути является интегралом в заданом интервале, деленом на значение интервала.
* Approximation to ~X points - апросимация значений по алгоритму дугласа-рамера-пекера с подбором нужного коэффициента сглаживания. Если на нужном интервале меньше значений чем X - добавлять в логи без сглаживания.

Генерируемы логи для различных периодов должны быть актуальны и включать значения для текущего интервала.

#### Модели логов

Своя коллекция для каждого периода (если необходимо).

* Raw
``` JSON
{
  hubid: {
    type: String,
    required: true
  },
  loggerid: {
    type: String,
    required: true,
    index: true
  },
  day: {
    type: Date,
    required: true,
    index: true,
    default: getDay
  },
  data: [
    {
      ts: {
        type: Date,
        required: true,
        index: true,
        default: Date.now
      },
      v: {
        type: Number,
        required: true
      },
      _id: false
    }
  ]
}
```
* Hour (см. raw)
* Day (см. raw)
* Week
``` Javascript
{
  hubid: {
    type: String,
    required: true
  },
  loggerid: {
    type: String,
    required: true,
    index: true
  },
  month: {
    type: Date,
    required: true,
    index: true,
    default: getMonth
  },
  data: [
    {
      ts: {
        type: Date,
        required: true,
        index: true,
        default: Date.now
      },
      v: {
        type: Number,
        required: true
      },
      _id: false
    }
  ]
}
```
* Month (см. week)
* Year (см. week)

