Modbus TCP** - драйвер для работы в режиме мастера по протоколу Modbus  через сеть TCP.


## Адресация устройств

ID - число от 0 до 255. 0 - широковещательный адресс, используется для массовой рассылки команд.
Зачастную в Modbus TCP игнорируется сервером, т.к. соединение изначально устанавливается точка-точка.
Используется только в концентраторах Modbus RTU/ASCII.

## Адресация данных

Логический адрес данных представляет собой шестизначное жесятичное число, где старшая цифра обозначает тип данных.

![](../images/modbus.png) {.img-responsive}

Для формирования физического адреса нужно:
1. Отбросить старшую цифру логического адреса.
2. Вычесть 1.

## Типы данных

* Bit - 1 coil/input
* Bit array - 1 holding/input register.
* Integer unsigned (int16) (по умолчанию) - 1 holding/input register
* Integer signed (uint16) - 1 holding/input register
* Long unsigned (int32) - 2 holding/input registers
* Long signed (uint32) - 2 holding/input registers
* Float - 2 holding/input registers

## Примеры устройств

[MO-TR8-SL](http://ru.unitecontrol.com/docs/devices/MO-TR8-SL.html)
[MO-DI4-MI](http://ru.unitecontrol.com/docs/devices/MO-DI4-MI.html)

## Драйвер

``` json
{
    "id" : "modbustcp",
    "name" : "Modbus TCP",
    "description" : "System modbus TCP driver",
    "templates" : {
        "settings" : {
            "ip" : {
                "required" : true,
                "type" : "string",
                "title" : "IP address"
            },
            "port" : {
                "required" : true,
                "type" : "number",
                "title" : "Port",
                "max" : 65535,
                "min" : 1
            },
            "address" : {
                "type" : "number",
                "title" : "Address",
                "min" : 0,
                "max" : 250
            }
        },
        "channels" : [
            {
                "title" : "Coil",
                "id" : "tmplc",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Address",
                        "min" : 1,
                        "max" : 9999
                    }
                }
            },
            {
                "title" : "Discrete input",
                "id" : "tmpldi",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Address",
                        "min" : 10001,
                        "max" : 19999
                    }
                }
            },
            {
                "title" : "Input registers",
                "id" : "tmplir",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Start address",
                        "min" : 30001,
                        "max" : 39999
                    },
                    "type" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Data type",
                        "default" : "int16",
                        "enum" : [
                            "int16",
                            "uint16",
                            "int32",
                            "uint32",
                            "float"
                        ]
                    },
                    "endianess" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Byte order",
                        "default" : "Little endian",
                        "enum" : [
                            "Big endian",
                            "Little endian"
                        ]
                    },
                    "wordswap" : {
                        "type" : "boolean",
                        "title" : "Swap words",
                        "default" : false
                    }
                }
            },
            {
                "title" : "Holding registers",
                "id" : "tmplhr",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Start address",
                        "min" : 40001,
                        "max" : 49999
                    },
                    "type" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Data type",
                        "default" : "int16",
                        "enum" : [
                            "int16",
                            "uint16",
                            "int32",
                            "uint32",
                            "float"
                        ]
                    },
                    "endianess" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Byte order",
                        "default" : "Little endian",
                        "enum" : [
                            "Big endian",
                            "Little endian"
                        ]
                    },
                    "wordswap" : {
                        "type" : "boolean",
                        "title" : "Swap words",
                        "default" : false
                    }
                }
            }
        ]
    },
    "autodiscover" : [],
    "hubid" : "any",
    "serial" : true,
}
```

### settings

* ip - tcp адрес устройства
* port - tcp порт устройства
* address - адрес modbus устройства (если необходимо)

### channels settings

* address - логический адрес регистра
* type - тип данных
* * int16 - один регистр, беззнаковое целое
* * uint16 - один регистр, целое со знаком
* * int32 - два регистра, беззнаковое целое
* * uint32 - два регистра, целое со знаком
* * float - два регистра, число с плавающией точкой
* endianess - порядок байт
* wordswap - смена стандартного порядка слов

Для "Discrete input" и "Coil" тип данных всегда бит - представлен в системе как 0/1.

[Modbus data encode](http://www.chipkin.com/how-real-floating-point-and-32-bit-data-is-encoded-in-modbus-rtu-messages/)
