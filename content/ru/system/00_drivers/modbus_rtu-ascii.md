**Modbus RTU/ASCII** - драйвер для работы в режиме мастера по протоколам Modbus RTU/ASCI.


## Адресация устройств

ID - число от 0 до 255. 0 - широковещательный адресс, используется для массовой рассылки команд.

## Адресация данных

Логический адрес данных представляет собой шестизначное жесятичное число, где старшая цифра обозначает тип данных. 

![](../images/modbus.png) {.img-responsive}

Для формирования физического адреса нужно:
1. Отбросить старшую цифру логического адреса.
2. Вычесть 1.

## Типы данных

* Bit - 1 coil/input
* Bit array - 1 holding/input register.
* Integer unsigned (int16) (по умолчанию) - 1 holding/input register
* Integer signed (uint16) - 1 holding/input register
* Long unsigned (int32) - 2 holding/input registers
* Long signed (uint32) - 2 holding/input registers
* Float - 2 holding/input registers

## Примеры устройств

[MO-TR8-SL](http://ru.unitecontrol.com/docs/devices/MO-TR8-SL.html)
[MO-DI4-MI](http://ru.unitecontrol.com/docs/devices/MO-DI4-MI.html)

## Драйвер

``` json
{
    "id" : "modbus",
    "name" : "Modbus RTU/ASCII",
    "description" : "System modbus rtu/ascii driver",
    "templates" : {
        "settings" : {
            "address" : {
                "required" : true,
                "type" : "number",
                "title" : "Address",
                "min" : 0,
                "max" : 250
            },
            "mode" : {
                "required" : true,
                "type" : "string",
                "title" : "Mode",
                "enum" : [
                    "RTU",
                    "ASCII"
                ],
                "default" : "RTU"
            }
        },
        "channels" : [
            {
                "title" : "Coil",
                "id" : "tmplc",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Address",
                        "min" : 1,
                        "max" : 9999
                    }
                }
            },
            {
                "title" : "Discrete input",
                "id" : "tmpldi",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Address",
                        "min" : 10001,
                        "max" : 19999
                    }
                }
            },
            {
                "title" : "Input registers",
                "id" : "tmplir",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Start address",
                        "min" : 30001,
                        "max" : 39999
                    },
                    "type" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Data type",
                        "default" : "int16",
                        "enum" : [
                            "int16",
                            "uint16",
                            "int32",
                            "uint32",
                            "float"
                        ]
                    },
                    "endianess" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Byte order",
                        "default" : "Little endian",
                        "enum" : [
                            "Big endian",
                            "Little endian"
                        ]
                    },
                    "wordswap" : {
                        "type" : "boolean",
                        "title" : "Swap words",
                        "default" : false
                    }
                }
            },
            {
                "title" : "Holding registers",
                "id" : "tmplhr",
                "settings" : {
                    "address" : {
                        "required" : true,
                        "type" : "number",
                        "title" : "Start address",
                        "min" : 40001,
                        "max" : 49999
                    },
                    "type" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Data type",
                        "default" : "int16",
                        "enum" : [
                            "int16",
                            "uint16",
                            "int32",
                            "uint32",
                            "float"
                        ]
                    },
                    "endianess" : {
                        "required" : true,
                        "type" : "string",
                        "title" : "Byte order",
                        "default" : "Little endian",
                        "enum" : [
                            "Big endian",
                            "Little endian"
                        ]
                    },
                    "wordswap" : {
                        "type" : "boolean",
                        "title" : "Swap words",
                        "default" : false
                    }
                }
            }
        ]
    },
    "autodiscover" : [],
    "hubid" : "any",
    "serial" : true,
}
```

### settings

* address - адрес modbus устройства
* mode - режим работы с modbus устройством

### channels settings

* address - логический адрес регистра
* type - тип данных
* * int16 - один регистр, беззнаковое целое
* * uint16 - один регистр, целое со знаком
* * int32 - два регистра, беззнаковое целое
* * uint32 - два регистра, целое со знаком
* * float - два регистра, число с плавающией точкой
* endianess - порядок байт
* wordswap - смена стандартного порядка слов

Для "Discrete input" и "Coil" тип данных всегда бит - представлен в системе как 0/1.

[Modbus data encode](http://www.chipkin.com/how-real-floating-point-and-32-bit-data-is-encoded-in-modbus-rtu-messages/)
