Драйвер для устройств на шине m-bus (meter-bus).

## Основные свойства и компоненты

### Поиск устройств ###

Предоставляет интерфейс для поиска устройств в шине m-bus и определения их типа и вывода всего дерева внутренних перменных. Пример возвращаемого объекта:
``` Javascript
[{
    id:"A2AB125F21251",
    data:[{
        name:"temperature",
        cast:"number"
    },{
        name:"fasttemp",
        cast:"number"
    }]
}]
```

#### Список автоматически определяемых полей ####

``` Javascript
```

### Схема настроек ###

``` Javascript
{
  "settings":{
    "id":{
      "title":"1-wire device id",
      "type": "string",
      "require":"true"
    }
  },
  "variables":[{
    "title":"Data channel"
    "name": {
        "type": "string",
        "title": "Variable name",
        "unique":true,
        "required":true
    },
    "data": {
        "type":"number",
        "title": "Data channel name",
        "required":true
    },
    "cast": {
        "type": "string",
        "enum":["boolean","number","bit array","string"],
        "title": "1-wire value type",
        "required":true,
        "default":"number"
    }
  }]
}
```