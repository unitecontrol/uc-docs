Драйвер для устройств на шине 1-wire. [Список поддерживаемых устройств](http://owfs.org/index.php?page=standard-devices)

## Драйвер

``` Javascript
{
    id: "onewire",
    hubid: "any",
    title: "1-wire",
    description: "System 1-wire driver",
    templates: {
        settings: {
            id:{
                title: "ID",
                type: "string",
                match:"^[0-9A-F./]+$",
                required: true
            }
        },
        variables: [{
            title: "Default variable",
            settings:{
                path:{
                    title: "Path",
                    type: "string",
                    match:"^[0-9A-F./]+$",
                    required: true
                },
                type:{
                    title: "Type",
                    type: "string",
                    enum:["number","string","bit","bitarray"],
                    default:"number",
                    required: true
                },
                interval:{
                    title: "Update interval (minute)",
                    type: "number",
                    max:60,
                    min:0,
                    default:5,
                    required: true
                }
            }
        }]
    },
    serial: false
}
```

#### Пример устройства

``` Javascript
{
    id: "deviceid",
    hubid: "hubid",
    name: "Office temperature",
    driver: "onewire",
    settings: {
        ip:"127.0.0.1",
        id:"10.67C6697351FF"
    },
    channels: [
        {
            variableid:"variableid",
            settings:{
              path:"/temperature",
              type:"number",
              interval:5
            }
        }
    ]
}
```

## Поиск устройств

Интерфейс discover возвращает массив настроек для устройств. Пример:

``` Javascript
[{
    hubid: "hubid",
    driver: "onewire",
    settings: {
        id:"10.67C6697351FF"
    },
    variables: [
        {
            settings:{
              path:"/temperature",
              type:"number",
              interval:5
            }
        }
    ]
}]
```