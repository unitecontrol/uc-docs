**Уведомление**

### Модель

``` Javascript
{
// id хаба
  hubid:{
    type:String,
    required:true,
    index:true
  },
  // отметка времени (когда уведомление было создано)
  timestamp:{
    type:Date,
    required:true,
    index:true,
    'default':Date.now
  },
  // тип уведомления
  type:{
    type:String,
    required:true,
    enum:['normrule', 'error', 'script']
  },
  // иконка для отображения в интерфейсе
  icon: String,
  // ед. измерения для отображения в интерфейсе
  units: String,
  // id ui (для перехода по клику, иконки и т.п.)
  ui: String,
  // Текст
  text: String,
  // СМС
  sms:{
    delivered:{
      type:Boolean,
      required:true,
      default:false
    },
    phones:[String]
  },
  // Email
  email:{
    delivered:{
      type:Boolean,
      required:true,
      'default':false
    },
    addresses:[String]
  },
  app:{
    delivered:{
      type: Boolean,
      required: true,
      default: false
    }
  }
}
```

### Методы компонента

* send(notification) - отправка уведомления (генерация и добавлению в базу на хабе)

*Модель* notification
``` Javascript
{
  type:{
    type:String,
    required:true,
    enum:['normrule', 'error', 'script']
  },
  icon: {
    type: String
  },
  units: {
    type: String
  },
  ui:{
    type:String
  },
  text:{
    type:String
  },
  sms:[{
    type: String
  }],
  email:[{
      type: String
  }]
}
```